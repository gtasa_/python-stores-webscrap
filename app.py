from flask import Flask, request, jsonify
from flask_cors import CORS, cross_origin

from functions.webscrap.search_functions import searchProductInMercadoLibre

app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'


@app.route('/')
def hello_world():
    return 'Hello, service working :)'

@app.route('/store/mercadolibre/<string:query>')
def mercadolibre(query):
    products = searchProductInMercadoLibre(query) # Look for PRODUCTS in webscraping MercadoLibre
    return jsonify({"products": products}), 200 # return sorted list