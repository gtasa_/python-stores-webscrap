from bs4 import BeautifulSoup
import requests

def getHTML(uri):
    url = uri
    HEADERS = {"content-type": "image/png"}
    html = requests.get(url, headers=HEADERS).text
    return BeautifulSoup(html, "html.parser")

    #this function returns the HTML which can be used with soup