from functions.webscrap.getHTML import getHTML
from operator import itemgetter

def searchProductInMercadoLibre(query):
    #list of all product found. At the end should look like this:
    # [
    #   {
    #       "product_name": ...,
    #       "product_price": ...,
    #       "product_image": ...,
    #   },
    #   {...},
    #   ...
    # ]
    product_list = []

    soup = getHTML(f'https://listado.mercadolibre.com.ec/{query}')

    # find the main element of the articles:
    HTMLproducts = soup.find_all('li', class_="ui-search-layout__item")

    print(f'Numero de productos encontrados: {len(HTMLproducts)}')

    # for each article found, we'll create a new product and add it to product_list list
    for index, product in enumerate(HTMLproducts):
        HTMLproduct_name = product.find('h2', class_="ui-search-item__title").text
        HTMLproduct_price = product.find('span', class_="price-tag-fraction").text.replace('.', '')
        HTMLproduct_image = product.find('img', class_="ui-search-result-image__element").get('data-src')
        HTMLproduct_link = product.find('a', class_="ui-search-link").get('href')

        product_data = {
            "product_name": HTMLproduct_name,
            "product_price": int(HTMLproduct_price),
            "product_image":  HTMLproduct_image,
            "product_link": HTMLproduct_link
        }

        product_list.append(product_data)
    
    sorted_list = sorted(product_list, key=itemgetter('product_price'))
    return sorted_list